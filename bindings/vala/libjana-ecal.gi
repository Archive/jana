<?xml version="1.0"?>
<api version="1.0">
	<namespace name="JanaEcal">
		<function name="utils_get_locations" symbol="jana_ecal_utils_get_locations">
			<return-type type="gchar**"/>
		</function>
		<function name="utils_guess_location" symbol="jana_ecal_utils_guess_location">
			<return-type type="gchar*"/>
		</function>
		<function name="utils_time_now" symbol="jana_ecal_utils_time_now">
			<return-type type="JanaTime*"/>
			<parameters>
				<parameter name="location" type="gchar*"/>
			</parameters>
		</function>
		<function name="utils_time_now_local" symbol="jana_ecal_utils_time_now_local">
			<return-type type="JanaTime*"/>
		</function>
		<function name="utils_time_today" symbol="jana_ecal_utils_time_today">
			<return-type type="JanaTime*"/>
			<parameters>
				<parameter name="location" type="gchar*"/>
			</parameters>
		</function>
		<object name="JanaEcalComponent" parent="GObject" type-name="JanaEcalComponent" get-type="jana_ecal_component_get_type">
			<implements>
				<interface name="JanaComponent"/>
			</implements>
			<method name="get_description" symbol="jana_ecal_component_get_description">
				<return-type type="gchar*"/>
				<parameters>
					<parameter name="self" type="JanaEcalComponent*"/>
				</parameters>
			</method>
			<method name="get_end" symbol="jana_ecal_component_get_end">
				<return-type type="JanaTime*"/>
				<parameters>
					<parameter name="self" type="JanaEcalComponent*"/>
				</parameters>
			</method>
			<method name="get_location" symbol="jana_ecal_component_get_location">
				<return-type type="gchar*"/>
				<parameters>
					<parameter name="self" type="JanaEcalComponent*"/>
				</parameters>
			</method>
			<method name="get_start" symbol="jana_ecal_component_get_start">
				<return-type type="JanaTime*"/>
				<parameters>
					<parameter name="self" type="JanaEcalComponent*"/>
				</parameters>
			</method>
			<method name="get_summary" symbol="jana_ecal_component_get_summary">
				<return-type type="gchar*"/>
				<parameters>
					<parameter name="self" type="JanaEcalComponent*"/>
				</parameters>
			</method>
			<!--<constructor name="new_from_ecalcomp" symbol="jana_ecal_component_new_from_ecalcomp">
				<return-type type="JanaComponent*"/>
				<parameters>
					<parameter name="component" type="ECalComponent*"/>
				</parameters>
			</constructor>-->
			<method name="set_description" symbol="jana_ecal_component_set_description">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaEcalComponent*"/>
					<parameter name="description" type="gchar*"/>
				</parameters>
			</method>
			<method name="set_end" symbol="jana_ecal_component_set_end">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaEcalComponent*"/>
					<parameter name="end" type="JanaTime*"/>
				</parameters>
			</method>
			<method name="set_location" symbol="jana_ecal_component_set_location">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaEcalComponent*"/>
					<parameter name="location" type="gchar*"/>
				</parameters>
			</method>
			<method name="set_start" symbol="jana_ecal_component_set_start">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaEcalComponent*"/>
					<parameter name="start" type="JanaTime*"/>
				</parameters>
			</method>
			<method name="set_summary" symbol="jana_ecal_component_set_summary">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaEcalComponent*"/>
					<parameter name="summary" type="gchar*"/>
				</parameters>
			</method>
			<!--<property name="ecalcomp" type="ECalComponent*" readable="1" writable="1" construct="0" construct-only="1"/>-->
		</object>
		<object name="JanaEcalEvent" parent="JanaEcalComponent" type-name="JanaEcalEvent" get-type="jana_ecal_event_get_type">
			<implements>
				<interface name="JanaComponent"/>
				<interface name="JanaEvent"/>
			</implements>
			<constructor name="new" symbol="jana_ecal_event_new">
				<return-type type="JanaEvent*"/>
			</constructor>
			<!--<constructor name="new_from_ecalcomp" symbol="jana_ecal_event_new_from_ecalcomp">
				<return-type type="JanaEvent*"/>
				<parameters>
					<parameter name="event" type="ECalComponent*"/>
				</parameters>
			</constructor>-->
		</object>
		<object name="JanaEcalNote" parent="JanaEcalComponent" type-name="JanaEcalNote" get-type="jana_ecal_note_get_type">
			<implements>
				<interface name="JanaComponent"/>
				<interface name="JanaNote"/>
			</implements>
			<constructor name="new" symbol="jana_ecal_note_new">
				<return-type type="JanaNote*"/>
			</constructor>
			<!--<constructor name="new_from_ecalcomp" symbol="jana_ecal_note_new_from_ecalcomp">
				<return-type type="JanaNote*"/>
				<parameters>
					<parameter name="note" type="ECalComponent*"/>
				</parameters>
			</constructor>-->
		</object>
		<object name="JanaEcalStore" parent="GObject" type-name="JanaEcalStore" get-type="jana_ecal_store_get_type">
			<implements>
				<interface name="JanaStore"/>
			</implements>
			<constructor name="new" symbol="jana_ecal_store_new">
				<return-type type="JanaStore*"/>
				<parameters>
					<parameter name="type" type="JanaComponentType"/>
				</parameters>
			</constructor>
			<constructor name="new_from_uri" symbol="jana_ecal_store_new_from_uri">
				<return-type type="JanaStore*"/>
				<parameters>
					<parameter name="uri" type="gchar*"/>
					<parameter name="type" type="JanaComponentType"/>
				</parameters>
			</constructor>
			<!--<property name="ecal" type="ECal*" readable="1" writable="1" construct="0" construct-only="1"/>-->
			<property name="type" type="gint" readable="1" writable="1" construct="0" construct-only="1"/>
		</object>
		<object name="JanaEcalStoreView" parent="GObject" type-name="JanaEcalStoreView" get-type="jana_ecal_store_view_get_type">
			<implements>
				<interface name="JanaStoreView"/>
			</implements>
			<constructor name="new" symbol="jana_ecal_store_view_new">
				<return-type type="JanaStoreView*"/>
				<parameters>
					<parameter name="store" type="JanaEcalStore*"/>
				</parameters>
			</constructor>
			<property name="end" type="GObject*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="parent" type="JanaEcalStore*" readable="1" writable="1" construct="0" construct-only="1"/>
			<property name="start" type="GObject*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="timeout" type="guint" readable="1" writable="1" construct="0" construct-only="0"/>
			<!--<property name="view" type="ECalView*" readable="1" writable="0" construct="0" construct-only="0"/>-->
		</object>
		<object name="JanaEcalTime" parent="GObject" type-name="JanaEcalTime" get-type="jana_ecal_time_get_type">
			<implements>
				<interface name="JanaTime"/>
			</implements>
			<method name="get_location" symbol="jana_ecal_time_get_location">
				<return-type type="gchar*"/>
				<parameters>
					<parameter name="self" type="JanaEcalTime*"/>
				</parameters>
			</method>
			<constructor name="new" symbol="jana_ecal_time_new">
				<return-type type="JanaTime*"/>
			</constructor>
			<!--<constructor name="new_from_ecaltime" symbol="jana_ecal_time_new_from_ecaltime">
				<return-type type="JanaTime*"/>
				<parameters>
					<parameter name="dt" type="ECalComponentDateTime*"/>
				</parameters>
			</constructor>-->
			<!--<constructor name="new_from_icaltime" symbol="jana_ecal_time_new_from_icaltime">
				<return-type type="JanaTime*"/>
				<parameters>
					<parameter name="time" type="icaltimetype*"/>
				</parameters>
			</constructor>-->
			<method name="set_location" symbol="jana_ecal_time_set_location">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaEcalTime*"/>
					<parameter name="location" type="gchar*"/>
				</parameters>
			</method>
			<!--<property name="icaltime" type="gpointer" readable="1" writable="1" construct="0" construct-only="1"/>-->
		</object>
		<constant name="JANA_ECAL_LOCATION_KEY" type="char*" value="/apps/evolution/calendar/display/timezone"/>
		<constant name="JANA_ECAL_LOCATION_KEY_DIR" type="char*" value="/apps/evolution/calendar/display"/>
	</namespace>
</api>
