<?xml version="1.0"?>
<api version="1.0">
	<namespace name="JanaGtk">
		<function name="utils_treeview_resize" symbol="jana_gtk_utils_treeview_resize">
			<return-type type="void"/>
			<parameters>
				<parameter name="tree_view" type="GtkWidget*"/>
				<parameter name="allocation" type="GtkAllocation*"/>
				<parameter name="cell_renderer" type="gpointer"/>
			</parameters>
		</function>
		<struct name="JanaGtkTreeLayoutCellInfo">
			<field name="row" type="GtkTreeRowReference*"/>
			<field name="x" type="gint"/>
			<field name="y" type="gint"/>
			<field name="width" type="gint"/>
			<field name="height" type="gint"/>
			<field name="real_x" type="gint"/>
			<field name="real_y" type="gint"/>
			<field name="real_width" type="gint"/>
			<field name="real_height" type="gint"/>
			<field name="sensitive" type="gboolean"/>
			<field name="renderer" type="GtkCellRenderer*"/>
			<field name="attributes" type="GList*"/>
		</struct>
		<object name="JanaGtkCellRendererEvent" parent="GtkCellRenderer" type-name="JanaGtkCellRendererEvent" get-type="jana_gtk_cell_renderer_event_get_type">
			<constructor name="new" symbol="jana_gtk_cell_renderer_event_new">
				<return-type type="GtkCellRenderer*"/>
			</constructor>
			<property name="categories" type="GStrv*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="category-color-hash" type="GHashTable*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="cell-width" type="gint" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="description" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="draw-box" type="gboolean" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="draw-detail" type="gboolean" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="draw-resize" type="gboolean" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="draw-text" type="gboolean" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="draw-time" type="gboolean" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="end" type="GObject*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="first-instance" type="gboolean" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="has-alarm" type="gboolean" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="has-recurrences" type="gboolean" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="last-instance" type="gboolean" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="location" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="row" type="gpointer" readable="0" writable="1" construct="0" construct-only="0"/>
			<property name="start" type="GObject*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="style-hint" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="summary" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="uid" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="xpad-inner" type="guint" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="ypad-inner" type="guint" readable="1" writable="1" construct="0" construct-only="0"/>
		</object>
		<object name="JanaGtkCellRendererNote" parent="GtkCellRenderer" type-name="JanaGtkCellRendererNote" get-type="jana_gtk_cell_renderer_note_get_type">
			<constructor name="new" symbol="jana_gtk_cell_renderer_note_new">
				<return-type type="GtkCellRenderer*"/>
			</constructor>
			<property name="author" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="body" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="categories" type="GStrv*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="category-color-hash" type="GHashTable*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="category-icon-hash" type="GHashTable*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="cell-width" type="gint" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="created" type="GObject*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="draw-box" type="gboolean" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="icon" type="GdkPixbuf*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="justify" type="GtkJustification" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="modified" type="GObject*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="recipient" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="show-author" type="gboolean" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="show-body" type="gboolean" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="show-created" type="gboolean" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="show-modified" type="gboolean" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="show-recipient" type="gboolean" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="style-hint" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="uid" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="xpad-inner" type="guint" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="ypad-inner" type="guint" readable="1" writable="1" construct="0" construct-only="0"/>
		</object>
		<object name="JanaGtkClock" parent="GtkEventBox" type-name="JanaGtkClock" get-type="jana_gtk_clock_get_type">
			<implements>
				<interface name="GtkBuildable"/>
				<interface name="AtkImplementor"/>
			</implements>
			<method name="get_buffer_time" symbol="jana_gtk_clock_get_buffer_time">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="self" type="JanaGtkClock*"/>
				</parameters>
			</method>
			<method name="get_digital" symbol="jana_gtk_clock_get_digital">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="self" type="JanaGtkClock*"/>
				</parameters>
			</method>
			<method name="get_draw_shadow" symbol="jana_gtk_clock_get_draw_shadow">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="self" type="JanaGtkClock*"/>
				</parameters>
			</method>
			<method name="get_show_seconds" symbol="jana_gtk_clock_get_show_seconds">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="self" type="JanaGtkClock*"/>
				</parameters>
			</method>
			<method name="get_time" symbol="jana_gtk_clock_get_time">
				<return-type type="JanaTime*"/>
				<parameters>
					<parameter name="self" type="JanaGtkClock*"/>
				</parameters>
			</method>
			<constructor name="new" symbol="jana_gtk_clock_new">
				<return-type type="GtkWidget*"/>
			</constructor>
			<method name="set_buffer_time" symbol="jana_gtk_clock_set_buffer_time">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkClock*"/>
					<parameter name="buffer_time" type="gboolean"/>
				</parameters>
			</method>
			<method name="set_digital" symbol="jana_gtk_clock_set_digital">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkClock*"/>
					<parameter name="digital" type="gboolean"/>
				</parameters>
			</method>
			<method name="set_draw_shadow" symbol="jana_gtk_clock_set_draw_shadow">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkClock*"/>
					<parameter name="draw_shadow" type="gboolean"/>
				</parameters>
			</method>
			<method name="set_show_seconds" symbol="jana_gtk_clock_set_show_seconds">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkClock*"/>
					<parameter name="show_seconds" type="gboolean"/>
				</parameters>
			</method>
			<method name="set_time" symbol="jana_gtk_clock_set_time">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkClock*"/>
					<parameter name="time" type="JanaTime*"/>
				</parameters>
			</method>
			<property name="buffer-time" type="gboolean" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="digital" type="gboolean" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="draw-shadow" type="gboolean" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="show-seconds" type="gboolean" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="time" type="GObject*" readable="1" writable="1" construct="0" construct-only="0"/>
			<signal name="clicked" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkClock*"/>
					<parameter name="event" type="gpointer"/>
				</parameters>
			</signal>
			<signal name="render-start" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkClock*"/>
				</parameters>
			</signal>
			<signal name="render-stop" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkClock*"/>
				</parameters>
			</signal>
		</object>
		<object name="JanaGtkDateTime" parent="GtkVBox" type-name="JanaGtkDateTime" get-type="jana_gtk_date_time_get_type">
			<implements>
				<interface name="GtkBuildable"/>
				<interface name="AtkImplementor"/>
			</implements>
			<method name="get_editable" symbol="jana_gtk_date_time_get_editable">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="self" type="JanaGtkDateTime*"/>
				</parameters>
			</method>
			<method name="get_time" symbol="jana_gtk_date_time_get_time">
				<return-type type="JanaTime*"/>
				<parameters>
					<parameter name="self" type="JanaGtkDateTime*"/>
				</parameters>
			</method>
			<constructor name="new" symbol="jana_gtk_date_time_new">
				<return-type type="GtkWidget*"/>
				<parameters>
					<parameter name="time" type="JanaTime*"/>
				</parameters>
			</constructor>
			<method name="set_editable" symbol="jana_gtk_date_time_set_editable">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkDateTime*"/>
					<parameter name="editable" type="gboolean"/>
				</parameters>
			</method>
			<method name="set_time" symbol="jana_gtk_date_time_set_time">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkDateTime*"/>
					<parameter name="time" type="JanaTime*"/>
				</parameters>
			</method>
			<property name="editable" type="gboolean" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="entry-format" type="gint" readable="1" writable="1" construct="0" construct-only="1"/>
			<property name="fast-timeout" type="gint" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="repeat" type="gint" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="time" type="GObject*" readable="1" writable="1" construct="1" construct-only="0"/>
			<property name="timeout" type="gint" readable="1" writable="1" construct="0" construct-only="0"/>
			<signal name="changed" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkDateTime*"/>
				</parameters>
			</signal>
		</object>
		<object name="JanaGtkDayView" parent="GtkEventBox" type-name="JanaGtkDayView" get-type="jana_gtk_day_view_get_type">
			<implements>
				<interface name="GtkBuildable"/>
				<interface name="AtkImplementor"/>
			</implements>
			<method name="add_store" symbol="jana_gtk_day_view_add_store">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkDayView*"/>
					<parameter name="store" type="JanaGtkEventStore*"/>
				</parameters>
			</method>
			<method name="get_24hr_cell_renderer" symbol="jana_gtk_day_view_get_24hr_cell_renderer">
				<return-type type="JanaGtkCellRendererEvent*"/>
				<parameters>
					<parameter name="self" type="JanaGtkDayView*"/>
				</parameters>
			</method>
			<method name="get_cell_renderer" symbol="jana_gtk_day_view_get_cell_renderer">
				<return-type type="JanaGtkCellRendererEvent*"/>
				<parameters>
					<parameter name="self" type="JanaGtkDayView*"/>
				</parameters>
			</method>
			<method name="get_cells" symbol="jana_gtk_day_view_get_cells">
				<return-type type="guint"/>
				<parameters>
					<parameter name="self" type="JanaGtkDayView*"/>
				</parameters>
			</method>
			<method name="get_range" symbol="jana_gtk_day_view_get_range">
				<return-type type="JanaDuration*"/>
				<parameters>
					<parameter name="self" type="JanaGtkDayView*"/>
				</parameters>
			</method>
			<method name="get_selected_event" symbol="jana_gtk_day_view_get_selected_event">
				<return-type type="GtkTreeRowReference*"/>
				<parameters>
					<parameter name="self" type="JanaGtkDayView*"/>
				</parameters>
			</method>
			<method name="get_selection" symbol="jana_gtk_day_view_get_selection">
				<return-type type="JanaDuration*"/>
				<parameters>
					<parameter name="self" type="JanaGtkDayView*"/>
				</parameters>
			</method>
			<method name="get_spacing" symbol="jana_gtk_day_view_get_spacing">
				<return-type type="guint"/>
				<parameters>
					<parameter name="self" type="JanaGtkDayView*"/>
				</parameters>
			</method>
			<method name="get_visible_ratio" symbol="jana_gtk_day_view_get_visible_ratio">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkDayView*"/>
					<parameter name="xratio" type="gdouble*"/>
					<parameter name="yratio" type="gdouble*"/>
				</parameters>
			</method>
			<constructor name="new" symbol="jana_gtk_day_view_new">
				<return-type type="GtkWidget*"/>
				<parameters>
					<parameter name="range" type="JanaDuration*"/>
					<parameter name="cells" type="guint"/>
				</parameters>
			</constructor>
			<method name="refilter" symbol="jana_gtk_day_view_refilter">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkDayView*"/>
				</parameters>
			</method>
			<method name="remove_store" symbol="jana_gtk_day_view_remove_store">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkDayView*"/>
					<parameter name="store" type="JanaGtkEventStore*"/>
				</parameters>
			</method>
			<method name="scroll_to_cell" symbol="jana_gtk_day_view_scroll_to_cell">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkDayView*"/>
					<parameter name="x" type="guint"/>
					<parameter name="y" type="guint"/>
				</parameters>
			</method>
			<method name="scroll_to_time" symbol="jana_gtk_day_view_scroll_to_time">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkDayView*"/>
					<parameter name="time" type="JanaTime*"/>
				</parameters>
			</method>
			<method name="set_active_range" symbol="jana_gtk_day_view_set_active_range">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkDayView*"/>
					<parameter name="range" type="JanaDuration*"/>
				</parameters>
			</method>
			<method name="set_cells" symbol="jana_gtk_day_view_set_cells">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkDayView*"/>
					<parameter name="cells" type="guint"/>
				</parameters>
			</method>
			<method name="set_highlighted_time" symbol="jana_gtk_day_view_set_highlighted_time">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkDayView*"/>
					<parameter name="time" type="JanaTime*"/>
				</parameters>
			</method>
			<method name="set_range" symbol="jana_gtk_day_view_set_range">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkDayView*"/>
					<parameter name="range" type="JanaDuration*"/>
				</parameters>
			</method>
			<method name="set_selected_event" symbol="jana_gtk_day_view_set_selected_event">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkDayView*"/>
					<parameter name="row" type="GtkTreeRowReference*"/>
				</parameters>
			</method>
			<method name="set_selection" symbol="jana_gtk_day_view_set_selection">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkDayView*"/>
					<parameter name="selection" type="JanaDuration*"/>
				</parameters>
			</method>
			<method name="set_spacing" symbol="jana_gtk_day_view_set_spacing">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkDayView*"/>
					<parameter name="spacing" type="guint"/>
				</parameters>
			</method>
			<method name="set_visible_func" symbol="jana_gtk_day_view_set_visible_func">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkDayView*"/>
					<parameter name="visible_cb" type="GtkTreeModelFilterVisibleFunc"/>
					<parameter name="data" type="gpointer"/>
				</parameters>
			</method>
			<method name="set_visible_ratio" symbol="jana_gtk_day_view_set_visible_ratio">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkDayView*"/>
					<parameter name="xratio" type="gdouble"/>
					<parameter name="yratio" type="gdouble"/>
				</parameters>
			</method>
			<property name="active-range" type="JanaDuration*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="cells" type="guint" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="highlighted-time" type="GObject*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="range" type="JanaDuration*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="selected-event" type="GtkTreeRowReference*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="selection" type="JanaDuration*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="spacing" type="guint" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="style-hint" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="xratio" type="gdouble" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="yratio" type="gdouble" readable="1" writable="1" construct="0" construct-only="0"/>
			<signal name="event-activated" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkDayView*"/>
					<parameter name="event" type="GtkTreeRowReference*"/>
				</parameters>
			</signal>
			<signal name="event-selected" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkDayView*"/>
					<parameter name="event" type="GtkTreeRowReference*"/>
				</parameters>
			</signal>
			<signal name="selection-changed" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkDayView*"/>
					<parameter name="range" type="JanaDuration*"/>
				</parameters>
			</signal>
			<signal name="set-scroll-adjustments" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkDayView*"/>
					<parameter name="hadjustment" type="GtkAdjustment*"/>
					<parameter name="vadjustment" type="GtkAdjustment*"/>
				</parameters>
			</signal>
		</object>
		<object name="JanaGtkEventList" parent="GtkTreeView" type-name="JanaGtkEventList" get-type="jana_gtk_event_list_get_type">
			<implements>
				<interface name="GtkBuildable"/>
				<interface name="AtkImplementor"/>
			</implements>
			<method name="add_store" symbol="jana_gtk_event_list_add_store">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkEventList*"/>
					<parameter name="store" type="JanaGtkEventStore*"/>
				</parameters>
			</method>
			<method name="get_filter" symbol="jana_gtk_event_list_get_filter">
				<return-type type="GtkTreeModelFilter*"/>
				<parameters>
					<parameter name="self" type="JanaGtkEventList*"/>
				</parameters>
			</method>
			<method name="get_show_headers" symbol="jana_gtk_event_list_get_show_headers">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="self" type="JanaGtkEventList*"/>
				</parameters>
			</method>
			<constructor name="new" symbol="jana_gtk_event_list_new">
				<return-type type="GtkWidget*"/>
			</constructor>
			<method name="refilter" symbol="jana_gtk_event_list_refilter">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkEventList*"/>
				</parameters>
			</method>
			<method name="remove_store" symbol="jana_gtk_event_list_remove_store">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkEventList*"/>
					<parameter name="store" type="JanaGtkEventStore*"/>
				</parameters>
			</method>
			<method name="set_show_headers" symbol="jana_gtk_event_list_set_show_headers">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkEventList*"/>
					<parameter name="show_headers" type="gboolean"/>
				</parameters>
			</method>
			<property name="column" type="GtkTreeViewColumn*" readable="1" writable="0" construct="0" construct-only="0"/>
			<property name="event-renderer" type="JanaGtkCellRendererEvent*" readable="1" writable="0" construct="0" construct-only="0"/>
			<property name="show-headers" type="gboolean" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="text-renderer" type="GtkCellRendererText*" readable="1" writable="0" construct="0" construct-only="0"/>
		</object>
		<object name="JanaGtkEventStore" parent="GtkListStore" type-name="JanaGtkEventStore" get-type="jana_gtk_event_store_get_type">
			<implements>
				<interface name="GtkTreeDragDest"/>
				<interface name="GtkTreeModel"/>
				<interface name="GtkBuildable"/>
				<interface name="GtkTreeSortable"/>
				<interface name="GtkTreeDragSource"/>
			</implements>
			<method name="get_store" symbol="jana_gtk_event_store_get_store">
				<return-type type="JanaStore*"/>
				<parameters>
					<parameter name="store" type="JanaGtkEventStore*"/>
				</parameters>
			</method>
			<method name="get_view" symbol="jana_gtk_event_store_get_view">
				<return-type type="JanaStoreView*"/>
				<parameters>
					<parameter name="store" type="JanaGtkEventStore*"/>
				</parameters>
			</method>
			<constructor name="new" symbol="jana_gtk_event_store_new">
				<return-type type="GtkTreeModel*"/>
			</constructor>
			<constructor name="new_full" symbol="jana_gtk_event_store_new_full">
				<return-type type="GtkTreeModel*"/>
				<parameters>
					<parameter name="view" type="JanaStoreView*"/>
					<parameter name="offset" type="glong"/>
				</parameters>
			</constructor>
			<method name="set_offset" symbol="jana_gtk_event_store_set_offset">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkEventStore*"/>
					<parameter name="offset" type="glong"/>
				</parameters>
			</method>
			<method name="set_view" symbol="jana_gtk_event_store_set_view">
				<return-type type="void"/>
				<parameters>
					<parameter name="store" type="JanaGtkEventStore*"/>
					<parameter name="view" type="JanaStoreView*"/>
				</parameters>
			</method>
			<property name="offset" type="glong" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="view" type="GObject*" readable="1" writable="1" construct="0" construct-only="0"/>
		</object>
		<object name="JanaGtkMonthView" parent="GtkEventBox" type-name="JanaGtkMonthView" get-type="jana_gtk_month_view_get_type">
			<implements>
				<interface name="GtkBuildable"/>
				<interface name="AtkImplementor"/>
			</implements>
			<method name="add_store" symbol="jana_gtk_month_view_add_store">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkMonthView*"/>
					<parameter name="store" type="JanaGtkEventStore*"/>
				</parameters>
			</method>
			<method name="get_cell_renderer" symbol="jana_gtk_month_view_get_cell_renderer">
				<return-type type="JanaGtkCellRendererEvent*"/>
				<parameters>
					<parameter name="self" type="JanaGtkMonthView*"/>
				</parameters>
			</method>
			<method name="get_month" symbol="jana_gtk_month_view_get_month">
				<return-type type="JanaTime*"/>
				<parameters>
					<parameter name="self" type="JanaGtkMonthView*"/>
				</parameters>
			</method>
			<method name="get_selection" symbol="jana_gtk_month_view_get_selection">
				<return-type type="JanaTime*"/>
				<parameters>
					<parameter name="self" type="JanaGtkMonthView*"/>
				</parameters>
			</method>
			<method name="get_spacing" symbol="jana_gtk_month_view_get_spacing">
				<return-type type="guint"/>
				<parameters>
					<parameter name="self" type="JanaGtkMonthView*"/>
				</parameters>
			</method>
			<constructor name="new" symbol="jana_gtk_month_view_new">
				<return-type type="GtkWidget*"/>
				<parameters>
					<parameter name="month" type="JanaTime*"/>
				</parameters>
			</constructor>
			<method name="refilter" symbol="jana_gtk_month_view_refilter">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkMonthView*"/>
				</parameters>
			</method>
			<method name="remove_store" symbol="jana_gtk_month_view_remove_store">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkMonthView*"/>
					<parameter name="store" type="JanaGtkEventStore*"/>
				</parameters>
			</method>
			<method name="set_highlighted_time" symbol="jana_gtk_month_view_set_highlighted_time">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkMonthView*"/>
					<parameter name="time" type="JanaTime*"/>
				</parameters>
			</method>
			<method name="set_month" symbol="jana_gtk_month_view_set_month">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkMonthView*"/>
					<parameter name="month" type="JanaTime*"/>
				</parameters>
			</method>
			<method name="set_selection" symbol="jana_gtk_month_view_set_selection">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkMonthView*"/>
					<parameter name="day" type="JanaTime*"/>
				</parameters>
			</method>
			<method name="set_spacing" symbol="jana_gtk_month_view_set_spacing">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkMonthView*"/>
					<parameter name="spacing" type="guint"/>
				</parameters>
			</method>
			<method name="set_visible_func" symbol="jana_gtk_month_view_set_visible_func">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkMonthView*"/>
					<parameter name="visible_cb" type="GtkTreeModelFilterVisibleFunc"/>
					<parameter name="data" type="gpointer"/>
				</parameters>
			</method>
			<property name="highlighted-time" type="GObject*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="month" type="GObject*" readable="1" writable="1" construct="1" construct-only="0"/>
			<property name="selection" type="GObject*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="spacing" type="guint" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="style-hint" type="char*" readable="1" writable="1" construct="0" construct-only="0"/>
			<signal name="selection-changed" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkMonthView*"/>
					<parameter name="day" type="GObject*"/>
				</parameters>
			</signal>
		</object>
		<object name="JanaGtkNoteStore" parent="GtkListStore" type-name="JanaGtkNoteStore" get-type="jana_gtk_note_store_get_type">
			<implements>
				<interface name="GtkTreeDragDest"/>
				<interface name="GtkTreeModel"/>
				<interface name="GtkBuildable"/>
				<interface name="GtkTreeSortable"/>
				<interface name="GtkTreeDragSource"/>
			</implements>
			<method name="get_store" symbol="jana_gtk_note_store_get_store">
				<return-type type="JanaStore*"/>
				<parameters>
					<parameter name="store" type="JanaGtkNoteStore*"/>
				</parameters>
			</method>
			<method name="get_view" symbol="jana_gtk_note_store_get_view">
				<return-type type="JanaStoreView*"/>
				<parameters>
					<parameter name="store" type="JanaGtkNoteStore*"/>
				</parameters>
			</method>
			<constructor name="new" symbol="jana_gtk_note_store_new">
				<return-type type="GtkTreeModel*"/>
			</constructor>
			<constructor name="new_full" symbol="jana_gtk_note_store_new_full">
				<return-type type="GtkTreeModel*"/>
				<parameters>
					<parameter name="view" type="JanaStoreView*"/>
				</parameters>
			</constructor>
			<method name="set_view" symbol="jana_gtk_note_store_set_view">
				<return-type type="void"/>
				<parameters>
					<parameter name="store" type="JanaGtkNoteStore*"/>
					<parameter name="view" type="JanaStoreView*"/>
				</parameters>
			</method>
			<property name="view" type="GObject*" readable="1" writable="1" construct="0" construct-only="0"/>
		</object>
		<object name="JanaGtkRecurrence" parent="GtkVBox" type-name="JanaGtkRecurrence" get-type="jana_gtk_recurrence_get_type">
			<implements>
				<interface name="GtkBuildable"/>
				<interface name="AtkImplementor"/>
			</implements>
			<method name="get_editable" symbol="jana_gtk_recurrence_get_editable">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="self" type="JanaGtkRecurrence*"/>
				</parameters>
			</method>
			<method name="get_recur" symbol="jana_gtk_recurrence_get_recur">
				<return-type type="JanaRecurrence*"/>
				<parameters>
					<parameter name="self" type="JanaGtkRecurrence*"/>
				</parameters>
			</method>
			<method name="get_time" symbol="jana_gtk_recurrence_get_time">
				<return-type type="JanaTime*"/>
				<parameters>
					<parameter name="self" type="JanaGtkRecurrence*"/>
				</parameters>
			</method>
			<constructor name="new" symbol="jana_gtk_recurrence_new">
				<return-type type="GtkWidget*"/>
			</constructor>
			<method name="set_editable" symbol="jana_gtk_recurrence_set_editable">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkRecurrence*"/>
					<parameter name="editable" type="gboolean"/>
				</parameters>
			</method>
			<method name="set_recur" symbol="jana_gtk_recurrence_set_recur">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkRecurrence*"/>
					<parameter name="recur" type="JanaRecurrence*"/>
				</parameters>
			</method>
			<method name="set_time" symbol="jana_gtk_recurrence_set_time">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkRecurrence*"/>
					<parameter name="time" type="JanaTime*"/>
				</parameters>
			</method>
			<property name="editable" type="gboolean" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="recur" type="JanaRecurrence*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="time" type="GObject*" readable="1" writable="1" construct="0" construct-only="0"/>
		</object>
		<object name="JanaGtkTreeLayout" parent="GtkEventBox" type-name="JanaGtkTreeLayout" get-type="jana_gtk_tree_layout_get_type">
			<implements>
				<interface name="GtkBuildable"/>
				<interface name="AtkImplementor"/>
			</implements>
			<method name="add_cell" symbol="jana_gtk_tree_layout_add_cell">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkTreeLayout*"/>
					<parameter name="row" type="GtkTreeRowReference*"/>
					<parameter name="x" type="gint"/>
					<parameter name="y" type="gint"/>
					<parameter name="width" type="gint"/>
					<parameter name="height" type="gint"/>
					<parameter name="renderer" type="GtkCellRenderer*"/>
				</parameters>
			</method>
			<method name="clear" symbol="jana_gtk_tree_layout_clear">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkTreeLayout*"/>
				</parameters>
			</method>
			<method name="get_cell" symbol="jana_gtk_tree_layout_get_cell">
				<return-type type="JanaGtkTreeLayoutCellInfo*"/>
				<parameters>
					<parameter name="self" type="JanaGtkTreeLayout*"/>
					<parameter name="row" type="GtkTreeRowReference*"/>
				</parameters>
			</method>
			<method name="get_cells" symbol="jana_gtk_tree_layout_get_cells">
				<return-type type="GList*"/>
				<parameters>
					<parameter name="self" type="JanaGtkTreeLayout*"/>
				</parameters>
			</method>
			<method name="get_selection" symbol="jana_gtk_tree_layout_get_selection">
				<return-type type="GList*"/>
				<parameters>
					<parameter name="self" type="JanaGtkTreeLayout*"/>
				</parameters>
			</method>
			<method name="move_cell" symbol="jana_gtk_tree_layout_move_cell">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkTreeLayout*"/>
					<parameter name="row" type="GtkTreeRowReference*"/>
					<parameter name="x" type="gint"/>
					<parameter name="y" type="gint"/>
					<parameter name="width" type="gint"/>
					<parameter name="height" type="gint"/>
				</parameters>
			</method>
			<constructor name="new" symbol="jana_gtk_tree_layout_new">
				<return-type type="GtkWidget*"/>
			</constructor>
			<method name="refilter" symbol="jana_gtk_tree_layout_refilter">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkTreeLayout*"/>
				</parameters>
			</method>
			<method name="remove_cell" symbol="jana_gtk_tree_layout_remove_cell">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkTreeLayout*"/>
					<parameter name="row" type="GtkTreeRowReference*"/>
				</parameters>
			</method>
			<method name="set_cell_sensitive" symbol="jana_gtk_tree_layout_set_cell_sensitive">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkTreeLayout*"/>
					<parameter name="row" type="GtkTreeRowReference*"/>
					<parameter name="sensitive" type="gboolean"/>
				</parameters>
			</method>
			<method name="set_selection" symbol="jana_gtk_tree_layout_set_selection">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkTreeLayout*"/>
					<parameter name="selection" type="GList*"/>
				</parameters>
			</method>
			<method name="set_visible_func" symbol="jana_gtk_tree_layout_set_visible_func">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkTreeLayout*"/>
					<parameter name="visible_cb" type="GtkTreeModelFilterVisibleFunc"/>
					<parameter name="data" type="gpointer"/>
				</parameters>
			</method>
			<property name="fill-height" type="gboolean" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="fill-width" type="gboolean" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="select-mode" type="gint" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="single-click" type="gboolean" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="sort-cb" type="gpointer" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="sort-data" type="gpointer" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="visible-cb" type="gpointer" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="visible-data" type="gpointer" readable="1" writable="1" construct="0" construct-only="0"/>
			<signal name="cell-activated" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkTreeLayout*"/>
					<parameter name="info" type="gpointer"/>
				</parameters>
			</signal>
			<signal name="selection-changed" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkTreeLayout*"/>
				</parameters>
			</signal>
			<vfunc name="add_cell">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkTreeLayout*"/>
					<parameter name="row" type="GtkTreeRowReference*"/>
					<parameter name="x" type="gint"/>
					<parameter name="y" type="gint"/>
					<parameter name="width" type="gint"/>
					<parameter name="height" type="gint"/>
					<parameter name="renderer" type="GtkCellRenderer*"/>
					<parameter name="args" type="va_list"/>
				</parameters>
			</vfunc>
			<vfunc name="clear">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkTreeLayout*"/>
				</parameters>
			</vfunc>
			<vfunc name="move_cell">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkTreeLayout*"/>
					<parameter name="row" type="GtkTreeRowReference*"/>
					<parameter name="x" type="gint"/>
					<parameter name="y" type="gint"/>
					<parameter name="width" type="gint"/>
					<parameter name="height" type="gint"/>
				</parameters>
			</vfunc>
			<vfunc name="remove_cell">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkTreeLayout*"/>
					<parameter name="row" type="GtkTreeRowReference*"/>
				</parameters>
			</vfunc>
		</object>
		<object name="JanaGtkWorldMap" parent="GtkEventBox" type-name="JanaGtkWorldMap" get-type="jana_gtk_world_map_get_type">
			<implements>
				<interface name="GtkBuildable"/>
				<interface name="AtkImplementor"/>
			</implements>
			<method name="add_marker" symbol="jana_gtk_world_map_add_marker">
				<return-type type="JanaGtkWorldMapMarker*"/>
				<parameters>
					<parameter name="self" type="JanaGtkWorldMap*"/>
					<parameter name="mark" type="JanaGtkWorldMapMarker*"/>
					<parameter name="lat" type="gdouble"/>
					<parameter name="lon" type="gdouble"/>
				</parameters>
			</method>
			<method name="get_height" symbol="jana_gtk_world_map_get_height">
				<return-type type="gint"/>
				<parameters>
					<parameter name="self" type="JanaGtkWorldMap*"/>
				</parameters>
			</method>
			<method name="get_latlon" symbol="jana_gtk_world_map_get_latlon">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkWorldMap*"/>
					<parameter name="x" type="gint"/>
					<parameter name="y" type="gint"/>
					<parameter name="lat" type="gdouble*"/>
					<parameter name="lon" type="gdouble*"/>
				</parameters>
			</method>
			<method name="get_markers" symbol="jana_gtk_world_map_get_markers">
				<return-type type="GList*"/>
				<parameters>
					<parameter name="self" type="JanaGtkWorldMap*"/>
				</parameters>
			</method>
			<method name="get_static" symbol="jana_gtk_world_map_get_static">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="self" type="JanaGtkWorldMap*"/>
				</parameters>
			</method>
			<method name="get_width" symbol="jana_gtk_world_map_get_width">
				<return-type type="gint"/>
				<parameters>
					<parameter name="self" type="JanaGtkWorldMap*"/>
				</parameters>
			</method>
			<method name="get_xy" symbol="jana_gtk_world_map_get_xy">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkWorldMap*"/>
					<parameter name="lat" type="gdouble"/>
					<parameter name="lon" type="gdouble"/>
					<parameter name="x" type="gint*"/>
					<parameter name="y" type="gint*"/>
				</parameters>
			</method>
			<method name="move_marker" symbol="jana_gtk_world_map_move_marker">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkWorldMap*"/>
					<parameter name="mark" type="JanaGtkWorldMapMarker*"/>
					<parameter name="lat" type="gdouble"/>
					<parameter name="lon" type="gdouble"/>
				</parameters>
			</method>
			<constructor name="new" symbol="jana_gtk_world_map_new">
				<return-type type="GtkWidget*"/>
			</constructor>
			<method name="remove_marker" symbol="jana_gtk_world_map_remove_marker">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkWorldMap*"/>
					<parameter name="mark" type="JanaGtkWorldMapMarker*"/>
				</parameters>
			</method>
			<method name="set_height" symbol="jana_gtk_world_map_set_height">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkWorldMap*"/>
					<parameter name="height" type="gint"/>
				</parameters>
			</method>
			<method name="set_static" symbol="jana_gtk_world_map_set_static">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkWorldMap*"/>
					<parameter name="set_static" type="gboolean"/>
				</parameters>
			</method>
			<method name="set_time" symbol="jana_gtk_world_map_set_time">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkWorldMap*"/>
					<parameter name="time" type="JanaTime*"/>
				</parameters>
			</method>
			<method name="set_width" symbol="jana_gtk_world_map_set_width">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkWorldMap*"/>
					<parameter name="width" type="gint"/>
				</parameters>
			</method>
			<property name="height" type="gint" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="static" type="gboolean" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="time" type="GObject*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="width" type="gint" readable="1" writable="1" construct="0" construct-only="0"/>
			<signal name="clicked" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkWorldMap*"/>
					<parameter name="event" type="gpointer"/>
				</parameters>
			</signal>
			<signal name="render-start" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkWorldMap*"/>
				</parameters>
			</signal>
			<signal name="render-stop" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkWorldMap*"/>
				</parameters>
			</signal>
		</object>
		<object name="JanaGtkWorldMapMarker" parent="GInitiallyUnowned" type-name="JanaGtkWorldMapMarker" get-type="jana_gtk_world_map_marker_get_type">
			<constructor name="new" symbol="jana_gtk_world_map_marker_new">
				<return-type type="JanaGtkWorldMapMarker*"/>
			</constructor>
			<method name="render" symbol="jana_gtk_world_map_marker_render">
				<return-type type="void"/>
				<parameters>
					<parameter name="marker" type="JanaGtkWorldMapMarker*"/>
					<parameter name="style" type="GtkStyle*"/>
					<parameter name="cr" type="cairo_t*"/>
					<parameter name="state" type="GtkStateType"/>
					<parameter name="x" type="gint"/>
					<parameter name="y" type="gint"/>
				</parameters>
			</method>
			<vfunc name="render">
				<return-type type="void"/>
				<parameters>
					<parameter name="marker" type="JanaGtkWorldMapMarker*"/>
					<parameter name="style" type="GtkStyle*"/>
					<parameter name="cr" type="cairo_t*"/>
					<parameter name="state" type="GtkStateType"/>
					<parameter name="x" type="gint"/>
					<parameter name="y" type="gint"/>
				</parameters>
			</vfunc>
			<field name="lat" type="gdouble"/>
			<field name="lon" type="gdouble"/>
		</object>
		<object name="JanaGtkWorldMapMarkerPixbuf" parent="JanaGtkWorldMapMarker" type-name="JanaGtkWorldMapMarkerPixbuf" get-type="jana_gtk_world_map_marker_pixbuf_get_type">
			<constructor name="new" symbol="jana_gtk_world_map_marker_pixbuf_new">
				<return-type type="JanaGtkWorldMapMarker*"/>
				<parameters>
					<parameter name="pixbuf" type="GdkPixbuf*"/>
				</parameters>
			</constructor>
			<field name="pixbuf" type="GdkPixbuf*"/>
		</object>
		<object name="JanaGtkYearView" parent="GtkTable" type-name="JanaGtkYearView" get-type="jana_gtk_year_view_get_type">
			<implements>
				<interface name="GtkBuildable"/>
				<interface name="AtkImplementor"/>
			</implements>
			<method name="add_store" symbol="jana_gtk_year_view_add_store">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkYearView*"/>
					<parameter name="store" type="JanaGtkEventStore*"/>
				</parameters>
			</method>
			<method name="get_selected_month" symbol="jana_gtk_year_view_get_selected_month">
				<return-type type="gint"/>
				<parameters>
					<parameter name="self" type="JanaGtkYearView*"/>
				</parameters>
			</method>
			<method name="get_year" symbol="jana_gtk_year_view_get_year">
				<return-type type="JanaTime*"/>
				<parameters>
					<parameter name="self" type="JanaGtkYearView*"/>
				</parameters>
			</method>
			<constructor name="new" symbol="jana_gtk_year_view_new">
				<return-type type="GtkWidget*"/>
				<parameters>
					<parameter name="months_per_row" type="guint"/>
					<parameter name="year" type="JanaTime*"/>
				</parameters>
			</constructor>
			<method name="remove_store" symbol="jana_gtk_year_view_remove_store">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkYearView*"/>
					<parameter name="store" type="JanaGtkEventStore*"/>
				</parameters>
			</method>
			<method name="set_highlighted_time" symbol="jana_gtk_year_view_set_highlighted_time">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkYearView*"/>
					<parameter name="time" type="JanaTime*"/>
				</parameters>
			</method>
			<method name="set_months_per_row" symbol="jana_gtk_year_view_set_months_per_row">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkYearView*"/>
					<parameter name="months_per_row" type="guint"/>
				</parameters>
			</method>
			<method name="set_selected_month" symbol="jana_gtk_year_view_set_selected_month">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkYearView*"/>
					<parameter name="month" type="gint"/>
				</parameters>
			</method>
			<method name="set_year" symbol="jana_gtk_year_view_set_year">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkYearView*"/>
					<parameter name="year" type="JanaTime*"/>
				</parameters>
			</method>
			<property name="highlighted-time" type="GObject*" readable="1" writable="1" construct="0" construct-only="0"/>
			<property name="months-per-row" type="guint" readable="0" writable="1" construct="1" construct-only="0"/>
			<property name="year" type="GObject*" readable="1" writable="1" construct="1" construct-only="0"/>
			<signal name="selection-changed" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaGtkYearView*"/>
					<parameter name="month" type="gint"/>
				</parameters>
			</signal>
		</object>
	</namespace>
</api>
