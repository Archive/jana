<?xml version="1.0"?>
<api version="1.0">
	<namespace name="Jana">
		<function name="exceptions_free" symbol="jana_exceptions_free">
			<return-type type="void"/>
			<parameters>
				<parameter name="exceptions" type="GList*"/>
			</parameters>
		</function>
		<function name="utils_ab_day" symbol="jana_utils_ab_day">
			<return-type type="gchar*"/>
			<parameters>
				<parameter name="day" type="guint"/>
			</parameters>
		</function>
		<function name="utils_component_has_category" symbol="jana_utils_component_has_category">
			<return-type type="gboolean"/>
			<parameters>
				<parameter name="component" type="JanaComponent*"/>
				<parameter name="category" type="gchar*"/>
			</parameters>
		</function>
		<function name="utils_component_insert_category" symbol="jana_utils_component_insert_category">
			<return-type type="void"/>
			<parameters>
				<parameter name="component" type="JanaComponent*"/>
				<parameter name="category" type="gchar*"/>
				<parameter name="position" type="gint"/>
			</parameters>
		</function>
		<function name="utils_component_remove_category" symbol="jana_utils_component_remove_category">
			<return-type type="gboolean"/>
			<parameters>
				<parameter name="component" type="JanaComponent*"/>
				<parameter name="category" type="gchar*"/>
			</parameters>
		</function>
		<function name="utils_duration_contains" symbol="jana_utils_duration_contains">
			<return-type type="gboolean"/>
			<parameters>
				<parameter name="duration" type="JanaDuration*"/>
				<parameter name="time" type="JanaTime*"/>
			</parameters>
		</function>
		<function name="utils_event_copy" symbol="jana_utils_event_copy">
			<return-type type="JanaEvent*"/>
			<parameters>
				<parameter name="source" type="JanaEvent*"/>
				<parameter name="dest" type="JanaEvent*"/>
			</parameters>
		</function>
		<function name="utils_event_get_instances" symbol="jana_utils_event_get_instances">
			<return-type type="GList*"/>
			<parameters>
				<parameter name="event" type="JanaEvent*"/>
				<parameter name="range_start" type="JanaTime*"/>
				<parameter name="range_end" type="JanaTime*"/>
				<parameter name="offset" type="glong"/>
			</parameters>
		</function>
		<function name="utils_get_local_tzname" symbol="jana_utils_get_local_tzname">
			<return-type type="gchar*"/>
		</function>
		<function name="utils_instance_list_free" symbol="jana_utils_instance_list_free">
			<return-type type="void"/>
			<parameters>
				<parameter name="instances" type="GList*"/>
			</parameters>
		</function>
		<function name="utils_note_copy" symbol="jana_utils_note_copy">
			<return-type type="JanaNote*"/>
			<parameters>
				<parameter name="source" type="JanaNote*"/>
				<parameter name="dest" type="JanaNote*"/>
			</parameters>
		</function>
		<function name="utils_recurrence_diff" symbol="jana_utils_recurrence_diff">
			<return-type type="gboolean"/>
			<parameters>
				<parameter name="r1" type="JanaRecurrence*"/>
				<parameter name="r2" type="JanaRecurrence*"/>
			</parameters>
		</function>
		<function name="utils_recurrence_to_string" symbol="jana_utils_recurrence_to_string">
			<return-type type="gchar*"/>
			<parameters>
				<parameter name="recur" type="JanaRecurrence*"/>
				<parameter name="start" type="JanaTime*"/>
			</parameters>
		</function>
		<function name="utils_strftime" symbol="jana_utils_strftime">
			<return-type type="gchar*"/>
			<parameters>
				<parameter name="time" type="JanaTime*"/>
				<parameter name="format" type="gchar*"/>
			</parameters>
		</function>
		<function name="utils_time_adjust" symbol="jana_utils_time_adjust">
			<return-type type="void"/>
			<parameters>
				<parameter name="time" type="JanaTime*"/>
				<parameter name="year" type="gint"/>
				<parameter name="month" type="gint"/>
				<parameter name="day" type="gint"/>
				<parameter name="hours" type="gint"/>
				<parameter name="minutes" type="gint"/>
				<parameter name="seconds" type="gint"/>
			</parameters>
		</function>
		<function name="utils_time_compare" symbol="jana_utils_time_compare">
			<return-type type="gint"/>
			<parameters>
				<parameter name="time1" type="JanaTime*"/>
				<parameter name="time2" type="JanaTime*"/>
				<parameter name="date_only" type="gboolean"/>
			</parameters>
		</function>
		<function name="utils_time_copy" symbol="jana_utils_time_copy">
			<return-type type="JanaTime*"/>
			<parameters>
				<parameter name="source" type="JanaTime*"/>
				<parameter name="dest" type="JanaTime*"/>
			</parameters>
		</function>
		<function name="utils_time_day_of_week" symbol="jana_utils_time_day_of_week">
			<return-type type="GDateWeekday"/>
			<parameters>
				<parameter name="time" type="JanaTime*"/>
			</parameters>
		</function>
		<function name="utils_time_day_of_year" symbol="jana_utils_time_day_of_year">
			<return-type type="guint"/>
			<parameters>
				<parameter name="time" type="JanaTime*"/>
			</parameters>
		</function>
		<function name="utils_time_daylight_hours" symbol="jana_utils_time_daylight_hours">
			<return-type type="gdouble"/>
			<parameters>
				<parameter name="latitude" type="gdouble"/>
				<parameter name="day_of_year" type="guint"/>
			</parameters>
		</function>
		<function name="utils_time_days_in_month" symbol="jana_utils_time_days_in_month">
			<return-type type="guint8"/>
			<parameters>
				<parameter name="year" type="guint16"/>
				<parameter name="month" type="guint8"/>
			</parameters>
		</function>
		<function name="utils_time_diff" symbol="jana_utils_time_diff">
			<return-type type="void"/>
			<parameters>
				<parameter name="t1" type="JanaTime*"/>
				<parameter name="t2" type="JanaTime*"/>
				<parameter name="year" type="gint*"/>
				<parameter name="month" type="gint*"/>
				<parameter name="day" type="gint*"/>
				<parameter name="hours" type="gint*"/>
				<parameter name="minutes" type="gint*"/>
				<parameter name="seconds" type="glong*"/>
			</parameters>
		</function>
		<function name="utils_time_is_leap_year" symbol="jana_utils_time_is_leap_year">
			<return-type type="gboolean"/>
			<parameters>
				<parameter name="year" type="guint16"/>
			</parameters>
		</function>
		<function name="utils_time_now" symbol="jana_utils_time_now">
			<return-type type="JanaTime*"/>
			<parameters>
				<parameter name="time" type="JanaTime*"/>
			</parameters>
		</function>
		<function name="utils_time_set_end_of_week" symbol="jana_utils_time_set_end_of_week">
			<return-type type="GDateWeekday"/>
			<parameters>
				<parameter name="end" type="JanaTime*"/>
			</parameters>
		</function>
		<function name="utils_time_set_start_of_week" symbol="jana_utils_time_set_start_of_week">
			<return-type type="GDateWeekday"/>
			<parameters>
				<parameter name="start" type="JanaTime*"/>
			</parameters>
		</function>
		<function name="utils_time_to_gdate" symbol="jana_utils_time_to_gdate">
			<return-type type="GDate*"/>
			<parameters>
				<parameter name="time" type="JanaTime*"/>
			</parameters>
		</function>
		<function name="utils_time_to_tm" symbol="jana_utils_time_to_tm">
			<return-type type="struct tm"/>
			<parameters>
				<parameter name="time" type="JanaTime*"/>
			</parameters>
		</function>
		<function name="utils_time_week_of_year" symbol="jana_utils_time_week_of_year">
			<return-type type="guint"/>
			<parameters>
				<parameter name="time" type="JanaTime*"/>
				<parameter name="week_starts_monday" type="gboolean"/>
			</parameters>
		</function>
		<struct name="JanaStoreViewMatch">
			<field name="field" type="JanaStoreViewField"/>
			<field name="data" type="gchar*"/>
		</struct>
		<boxed name="JanaDuration" type-name="JanaDuration" get-type="jana_duration_get_type">
			<method name="copy" symbol="jana_duration_copy">
				<return-type type="JanaDuration*"/>
				<parameters>
					<parameter name="duration" type="JanaDuration*"/>
				</parameters>
			</method>
			<method name="free" symbol="jana_duration_free">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaDuration*"/>
				</parameters>
			</method>
			<constructor name="new" symbol="jana_duration_new">
				<return-type type="JanaDuration*"/>
				<parameters>
					<parameter name="start" type="JanaTime*"/>
					<parameter name="end" type="JanaTime*"/>
				</parameters>
			</constructor>
			<method name="set_end" symbol="jana_duration_set_end">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaDuration*"/>
					<parameter name="end" type="JanaTime*"/>
				</parameters>
			</method>
			<method name="set_start" symbol="jana_duration_set_start">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaDuration*"/>
					<parameter name="start" type="JanaTime*"/>
				</parameters>
			</method>
			<method name="valid" symbol="jana_duration_valid">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="self" type="JanaDuration*"/>
				</parameters>
			</method>
			<field name="start" type="JanaTime*"/>
			<field name="end" type="JanaTime*"/>
		</boxed>
		<boxed name="JanaRecurrence" type-name="JanaRecurrence" get-type="jana_recurrence_get_type">
			<method name="copy" symbol="jana_recurrence_copy">
				<return-type type="JanaRecurrence*"/>
				<parameters>
					<parameter name="recurrence" type="JanaRecurrence*"/>
				</parameters>
			</method>
			<method name="free" symbol="jana_recurrence_free">
				<return-type type="void"/>
				<parameters>
					<parameter name="recurrence" type="JanaRecurrence*"/>
				</parameters>
			</method>
			<constructor name="new" symbol="jana_recurrence_new">
				<return-type type="JanaRecurrence*"/>
			</constructor>
			<field name="type" type="JanaRecurrenceType"/>
			<field name="interval" type="gint"/>
			<field name="week_days" type="gboolean[]"/>
			<field name="by_date" type="gboolean"/>
			<field name="end" type="JanaTime*"/>
		</boxed>
		<enum name="JanaComponentType">
			<member name="JANA_COMPONENT_NULL" value="0"/>
			<member name="JANA_COMPONENT_EVENT" value="1"/>
			<member name="JANA_COMPONENT_NOTE" value="2"/>
			<member name="JANA_COMPONENT_TASK" value="3"/>
		</enum>
		<enum name="JanaRecurrenceType">
			<member name="JANA_RECURRENCE_DAILY" value="0"/>
			<member name="JANA_RECURRENCE_WEEKLY" value="1"/>
			<member name="JANA_RECURRENCE_MONTHLY" value="2"/>
			<member name="JANA_RECURRENCE_YEARLY" value="3"/>
		</enum>
		<enum name="JanaStoreViewField">
			<member name="JANA_STORE_VIEW_SUMMARY" value="0"/>
			<member name="JANA_STORE_VIEW_LOCATION" value="1"/>
			<member name="JANA_STORE_VIEW_DESCRIPTION" value="2"/>
			<member name="JANA_STORE_VIEW_AUTHOR" value="3"/>
			<member name="JANA_STORE_VIEW_RECIPIENT" value="4"/>
			<member name="JANA_STORE_VIEW_BODY" value="5"/>
			<member name="JANA_STORE_VIEW_CATEGORY" value="6"/>
			<member name="JANA_STORE_VIEW_ANYFIELD" value="7"/>
		</enum>
		<interface name="JanaComponent" type-name="JanaComponent" get-type="jana_component_get_type">
			<requires>
				<interface name="GObject"/>
			</requires>
			<method name="get_categories" symbol="jana_component_get_categories">
				<return-type type="gchar**"/>
				<parameters>
					<parameter name="self" type="JanaComponent*"/>
				</parameters>
			</method>
			<method name="get_component_type" symbol="jana_component_get_component_type">
				<return-type type="JanaComponentType"/>
				<parameters>
					<parameter name="self" type="JanaComponent*"/>
				</parameters>
			</method>
			<method name="get_custom_prop" symbol="jana_component_get_custom_prop">
				<return-type type="gchar*"/>
				<parameters>
					<parameter name="self" type="JanaComponent*"/>
					<parameter name="name" type="gchar*"/>
				</parameters>
			</method>
			<method name="get_custom_props_list" symbol="jana_component_get_custom_props_list">
				<return-type type="GList*"/>
				<parameters>
					<parameter name="self" type="JanaComponent*"/>
				</parameters>
			</method>
			<method name="get_uid" symbol="jana_component_get_uid">
				<return-type type="gchar*"/>
				<parameters>
					<parameter name="self" type="JanaComponent*"/>
				</parameters>
			</method>
			<method name="is_fully_represented" symbol="jana_component_is_fully_represented">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="self" type="JanaComponent*"/>
				</parameters>
			</method>
			<method name="props_list_free" symbol="jana_component_props_list_free">
				<return-type type="void"/>
				<parameters>
					<parameter name="props" type="GList*"/>
				</parameters>
			</method>
			<method name="set_categories" symbol="jana_component_set_categories">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaComponent*"/>
					<parameter name="categories" type="gchar**"/>
				</parameters>
			</method>
			<method name="set_custom_prop" symbol="jana_component_set_custom_prop">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="self" type="JanaComponent*"/>
					<parameter name="name" type="gchar*"/>
					<parameter name="value" type="gchar*"/>
				</parameters>
			</method>
			<method name="supports_custom_props" symbol="jana_component_supports_custom_props">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="self" type="JanaComponent*"/>
				</parameters>
			</method>
			<vfunc name="get_categories">
				<return-type type="gchar**"/>
				<parameters>
					<parameter name="self" type="JanaComponent*"/>
				</parameters>
			</vfunc>
			<vfunc name="get_component_type">
				<return-type type="JanaComponentType"/>
				<parameters>
					<parameter name="self" type="JanaComponent*"/>
				</parameters>
			</vfunc>
			<vfunc name="get_custom_prop">
				<return-type type="gchar*"/>
				<parameters>
					<parameter name="self" type="JanaComponent*"/>
					<parameter name="name" type="gchar*"/>
				</parameters>
			</vfunc>
			<vfunc name="get_custom_props_list">
				<return-type type="GList*"/>
				<parameters>
					<parameter name="self" type="JanaComponent*"/>
				</parameters>
			</vfunc>
			<vfunc name="get_uid">
				<return-type type="gchar*"/>
				<parameters>
					<parameter name="self" type="JanaComponent*"/>
				</parameters>
			</vfunc>
			<vfunc name="is_fully_represented">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="self" type="JanaComponent*"/>
				</parameters>
			</vfunc>
			<vfunc name="set_categories">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaComponent*"/>
					<parameter name="categories" type="gchar**"/>
				</parameters>
			</vfunc>
			<vfunc name="set_custom_prop">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="self" type="JanaComponent*"/>
					<parameter name="name" type="gchar*"/>
					<parameter name="value" type="gchar*"/>
				</parameters>
			</vfunc>
			<vfunc name="supports_custom_props">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="self" type="JanaComponent*"/>
				</parameters>
			</vfunc>
		</interface>
		<interface name="JanaEvent" type-name="JanaEvent" get-type="jana_event_get_type">
			<requires>
				<interface name="GObject"/>
			</requires>
			<method name="get_alarm_time" symbol="jana_event_get_alarm_time">
				<return-type type="JanaTime*"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
				</parameters>
			</method>
			<method name="get_categories" symbol="jana_event_get_categories">
				<return-type type="gchar**"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
				</parameters>
			</method>
			<method name="get_description" symbol="jana_event_get_description">
				<return-type type="gchar*"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
				</parameters>
			</method>
			<method name="get_end" symbol="jana_event_get_end">
				<return-type type="JanaTime*"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
				</parameters>
			</method>
			<method name="get_exceptions" symbol="jana_event_get_exceptions">
				<return-type type="GList*"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
				</parameters>
			</method>
			<method name="get_location" symbol="jana_event_get_location">
				<return-type type="gchar*"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
				</parameters>
			</method>
			<method name="get_recurrence" symbol="jana_event_get_recurrence">
				<return-type type="JanaRecurrence*"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
				</parameters>
			</method>
			<method name="get_start" symbol="jana_event_get_start">
				<return-type type="JanaTime*"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
				</parameters>
			</method>
			<method name="get_summary" symbol="jana_event_get_summary">
				<return-type type="gchar*"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
				</parameters>
			</method>
			<method name="has_alarm" symbol="jana_event_has_alarm">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
				</parameters>
			</method>
			<method name="has_exceptions" symbol="jana_event_has_exceptions">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
				</parameters>
			</method>
			<method name="has_recurrence" symbol="jana_event_has_recurrence">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
				</parameters>
			</method>
			<method name="set_alarm" symbol="jana_event_set_alarm">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
					<parameter name="time" type="JanaTime*"/>
				</parameters>
			</method>
			<method name="set_categories" symbol="jana_event_set_categories">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
					<parameter name="categories" type="gchar**"/>
				</parameters>
			</method>
			<method name="set_description" symbol="jana_event_set_description">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
					<parameter name="description" type="gchar*"/>
				</parameters>
			</method>
			<method name="set_end" symbol="jana_event_set_end">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
					<parameter name="end" type="JanaTime*"/>
				</parameters>
			</method>
			<method name="set_exceptions" symbol="jana_event_set_exceptions">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
					<parameter name="exceptions" type="GList*"/>
				</parameters>
			</method>
			<method name="set_location" symbol="jana_event_set_location">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
					<parameter name="location" type="gchar*"/>
				</parameters>
			</method>
			<method name="set_recurrence" symbol="jana_event_set_recurrence">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
					<parameter name="recurrence" type="JanaRecurrence*"/>
				</parameters>
			</method>
			<method name="set_start" symbol="jana_event_set_start">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
					<parameter name="start" type="JanaTime*"/>
				</parameters>
			</method>
			<method name="set_summary" symbol="jana_event_set_summary">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
					<parameter name="summary" type="gchar*"/>
				</parameters>
			</method>
			<method name="supports_alarm" symbol="jana_event_supports_alarm">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
				</parameters>
			</method>
			<method name="supports_exceptions" symbol="jana_event_supports_exceptions">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
				</parameters>
			</method>
			<method name="supports_recurrence" symbol="jana_event_supports_recurrence">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
				</parameters>
			</method>
			<vfunc name="get_alarm_time">
				<return-type type="JanaTime*"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
				</parameters>
			</vfunc>
			<vfunc name="get_description">
				<return-type type="gchar*"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
				</parameters>
			</vfunc>
			<vfunc name="get_end">
				<return-type type="JanaTime*"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
				</parameters>
			</vfunc>
			<vfunc name="get_exceptions">
				<return-type type="GList*"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
				</parameters>
			</vfunc>
			<vfunc name="get_location">
				<return-type type="gchar*"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
				</parameters>
			</vfunc>
			<vfunc name="get_recurrence">
				<return-type type="JanaRecurrence*"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
				</parameters>
			</vfunc>
			<vfunc name="get_start">
				<return-type type="JanaTime*"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
				</parameters>
			</vfunc>
			<vfunc name="get_summary">
				<return-type type="gchar*"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
				</parameters>
			</vfunc>
			<vfunc name="has_alarm">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
				</parameters>
			</vfunc>
			<vfunc name="has_exceptions">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
				</parameters>
			</vfunc>
			<vfunc name="has_recurrence">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
				</parameters>
			</vfunc>
			<vfunc name="set_alarm">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
					<parameter name="time" type="JanaTime*"/>
				</parameters>
			</vfunc>
			<vfunc name="set_description">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
					<parameter name="description" type="gchar*"/>
				</parameters>
			</vfunc>
			<vfunc name="set_end">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
					<parameter name="end" type="JanaTime*"/>
				</parameters>
			</vfunc>
			<vfunc name="set_exceptions">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
					<parameter name="exceptions" type="GList*"/>
				</parameters>
			</vfunc>
			<vfunc name="set_location">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
					<parameter name="location" type="gchar*"/>
				</parameters>
			</vfunc>
			<vfunc name="set_recurrence">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
					<parameter name="recurrence" type="JanaRecurrence*"/>
				</parameters>
			</vfunc>
			<vfunc name="set_start">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
					<parameter name="start" type="JanaTime*"/>
				</parameters>
			</vfunc>
			<vfunc name="set_summary">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
					<parameter name="summary" type="gchar*"/>
				</parameters>
			</vfunc>
			<vfunc name="supports_alarm">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
				</parameters>
			</vfunc>
			<vfunc name="supports_exceptions">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
				</parameters>
			</vfunc>
			<vfunc name="supports_recurrence">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="self" type="JanaEvent*"/>
				</parameters>
			</vfunc>
		</interface>
		<interface name="JanaNote" type-name="JanaNote" get-type="jana_note_get_type">
			<requires>
				<interface name="GObject"/>
			</requires>
			<method name="get_author" symbol="jana_note_get_author">
				<return-type type="gchar*"/>
				<parameters>
					<parameter name="note" type="JanaNote*"/>
				</parameters>
			</method>
			<method name="get_body" symbol="jana_note_get_body">
				<return-type type="gchar*"/>
				<parameters>
					<parameter name="note" type="JanaNote*"/>
				</parameters>
			</method>
			<method name="get_creation_time" symbol="jana_note_get_creation_time">
				<return-type type="JanaTime*"/>
				<parameters>
					<parameter name="note" type="JanaNote*"/>
				</parameters>
			</method>
			<method name="get_modified_time" symbol="jana_note_get_modified_time">
				<return-type type="JanaTime*"/>
				<parameters>
					<parameter name="note" type="JanaNote*"/>
				</parameters>
			</method>
			<method name="get_recipient" symbol="jana_note_get_recipient">
				<return-type type="gchar*"/>
				<parameters>
					<parameter name="note" type="JanaNote*"/>
				</parameters>
			</method>
			<method name="set_author" symbol="jana_note_set_author">
				<return-type type="void"/>
				<parameters>
					<parameter name="note" type="JanaNote*"/>
					<parameter name="author" type="gchar*"/>
				</parameters>
			</method>
			<method name="set_body" symbol="jana_note_set_body">
				<return-type type="void"/>
				<parameters>
					<parameter name="note" type="JanaNote*"/>
					<parameter name="body" type="gchar*"/>
				</parameters>
			</method>
			<method name="set_creation_time" symbol="jana_note_set_creation_time">
				<return-type type="void"/>
				<parameters>
					<parameter name="note" type="JanaNote*"/>
					<parameter name="time" type="JanaTime*"/>
				</parameters>
			</method>
			<method name="set_recipient" symbol="jana_note_set_recipient">
				<return-type type="void"/>
				<parameters>
					<parameter name="note" type="JanaNote*"/>
					<parameter name="recipient" type="gchar*"/>
				</parameters>
			</method>
			<vfunc name="get_author">
				<return-type type="gchar*"/>
				<parameters>
					<parameter name="self" type="JanaNote*"/>
				</parameters>
			</vfunc>
			<vfunc name="get_body">
				<return-type type="gchar*"/>
				<parameters>
					<parameter name="self" type="JanaNote*"/>
				</parameters>
			</vfunc>
			<vfunc name="get_creation_time">
				<return-type type="JanaTime*"/>
				<parameters>
					<parameter name="self" type="JanaNote*"/>
				</parameters>
			</vfunc>
			<vfunc name="get_modified_time">
				<return-type type="JanaTime*"/>
				<parameters>
					<parameter name="self" type="JanaNote*"/>
				</parameters>
			</vfunc>
			<vfunc name="get_recipient">
				<return-type type="gchar*"/>
				<parameters>
					<parameter name="self" type="JanaNote*"/>
				</parameters>
			</vfunc>
			<vfunc name="set_author">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaNote*"/>
					<parameter name="author" type="gchar*"/>
				</parameters>
			</vfunc>
			<vfunc name="set_body">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaNote*"/>
					<parameter name="body" type="gchar*"/>
				</parameters>
			</vfunc>
			<vfunc name="set_creation_time">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaNote*"/>
					<parameter name="time" type="JanaTime*"/>
				</parameters>
			</vfunc>
			<vfunc name="set_recipient">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaNote*"/>
					<parameter name="recipient" type="gchar*"/>
				</parameters>
			</vfunc>
		</interface>
		<interface name="JanaStore" type-name="JanaStore" get-type="jana_store_get_type">
			<requires>
				<interface name="GObject"/>
			</requires>
			<method name="add_component" symbol="jana_store_add_component">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaStore*"/>
					<parameter name="comp" type="JanaComponent*"/>
				</parameters>
			</method>
			<method name="get_component" symbol="jana_store_get_component">
				<return-type type="JanaComponent*"/>
				<parameters>
					<parameter name="self" type="JanaStore*"/>
					<parameter name="uid" type="gchar*"/>
				</parameters>
			</method>
			<method name="get_view" symbol="jana_store_get_view">
				<return-type type="JanaStoreView*"/>
				<parameters>
					<parameter name="self" type="JanaStore*"/>
				</parameters>
			</method>
			<method name="modify_component" symbol="jana_store_modify_component">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaStore*"/>
					<parameter name="comp" type="JanaComponent*"/>
				</parameters>
			</method>
			<method name="open" symbol="jana_store_open">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaStore*"/>
				</parameters>
			</method>
			<method name="remove_component" symbol="jana_store_remove_component">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaStore*"/>
					<parameter name="comp" type="JanaComponent*"/>
				</parameters>
			</method>
			<signal name="opened" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaStore*"/>
				</parameters>
			</signal>
			<vfunc name="add_component">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaStore*"/>
					<parameter name="comp" type="JanaComponent*"/>
				</parameters>
			</vfunc>
			<vfunc name="get_component">
				<return-type type="JanaComponent*"/>
				<parameters>
					<parameter name="self" type="JanaStore*"/>
					<parameter name="uid" type="gchar*"/>
				</parameters>
			</vfunc>
			<vfunc name="get_view">
				<return-type type="JanaStoreView*"/>
				<parameters>
					<parameter name="self" type="JanaStore*"/>
				</parameters>
			</vfunc>
			<vfunc name="modify_component">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaStore*"/>
					<parameter name="comp" type="JanaComponent*"/>
				</parameters>
			</vfunc>
			<vfunc name="open">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaStore*"/>
				</parameters>
			</vfunc>
			<vfunc name="remove_component">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaStore*"/>
					<parameter name="comp" type="JanaComponent*"/>
				</parameters>
			</vfunc>
		</interface>
		<interface name="JanaStoreView" type-name="JanaStoreView" get-type="jana_store_view_get_type">
			<requires>
				<interface name="GObject"/>
			</requires>
			<method name="add_match" symbol="jana_store_view_add_match">
				<return-type type="JanaStoreViewMatch*"/>
				<parameters>
					<parameter name="self" type="JanaStoreView*"/>
					<parameter name="field" type="JanaStoreViewField"/>
					<parameter name="data" type="gchar*"/>
				</parameters>
			</method>
			<method name="clear_matches" symbol="jana_store_view_clear_matches">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaStoreView*"/>
				</parameters>
			</method>
			<method name="get_matches" symbol="jana_store_view_get_matches">
				<return-type type="GList*"/>
				<parameters>
					<parameter name="self" type="JanaStoreView*"/>
				</parameters>
			</method>
			<method name="get_range" symbol="jana_store_view_get_range">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaStoreView*"/>
					<parameter name="start" type="JanaTime**"/>
					<parameter name="end" type="JanaTime**"/>
				</parameters>
			</method>
			<method name="get_store" symbol="jana_store_view_get_store">
				<return-type type="JanaStore*"/>
				<parameters>
					<parameter name="self" type="JanaStoreView*"/>
				</parameters>
			</method>
			<method name="remove_match" symbol="jana_store_view_remove_match">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaStoreView*"/>
					<parameter name="match" type="JanaStoreViewMatch*"/>
				</parameters>
			</method>
			<method name="set_range" symbol="jana_store_view_set_range">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaStoreView*"/>
					<parameter name="start" type="JanaTime*"/>
					<parameter name="end" type="JanaTime*"/>
				</parameters>
			</method>
			<method name="start" symbol="jana_store_view_start">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaStoreView*"/>
				</parameters>
			</method>
			<signal name="added" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaStoreView*"/>
					<parameter name="components" type="gpointer"/>
				</parameters>
			</signal>
			<signal name="modified" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaStoreView*"/>
					<parameter name="components" type="gpointer"/>
				</parameters>
			</signal>
			<signal name="progress" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaStoreView*"/>
					<parameter name="percent" type="gint"/>
				</parameters>
			</signal>
			<signal name="removed" when="LAST">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaStoreView*"/>
					<parameter name="uids" type="gpointer"/>
				</parameters>
			</signal>
			<vfunc name="add_match">
				<return-type type="JanaStoreViewMatch*"/>
				<parameters>
					<parameter name="self" type="JanaStoreView*"/>
					<parameter name="field" type="JanaStoreViewField"/>
					<parameter name="data" type="gchar*"/>
				</parameters>
			</vfunc>
			<vfunc name="clear_matches">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaStoreView*"/>
				</parameters>
			</vfunc>
			<vfunc name="get_matches">
				<return-type type="GList*"/>
				<parameters>
					<parameter name="self" type="JanaStoreView*"/>
				</parameters>
			</vfunc>
			<vfunc name="get_range">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaStoreView*"/>
					<parameter name="start" type="JanaTime**"/>
					<parameter name="end" type="JanaTime**"/>
				</parameters>
			</vfunc>
			<vfunc name="get_store">
				<return-type type="JanaStore*"/>
				<parameters>
					<parameter name="self" type="JanaStoreView*"/>
				</parameters>
			</vfunc>
			<vfunc name="remove_match">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaStoreView*"/>
					<parameter name="match" type="JanaStoreViewMatch*"/>
				</parameters>
			</vfunc>
			<vfunc name="set_range">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaStoreView*"/>
					<parameter name="start" type="JanaTime*"/>
					<parameter name="end" type="JanaTime*"/>
				</parameters>
			</vfunc>
			<vfunc name="start">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaStoreView*"/>
				</parameters>
			</vfunc>
		</interface>
		<interface name="JanaTask" type-name="JanaTask" get-type="jana_task_get_type">
			<requires>
				<interface name="GObject"/>
			</requires>
		</interface>
		<interface name="JanaTime" type-name="JanaTime" get-type="jana_time_get_type">
			<requires>
				<interface name="GObject"/>
			</requires>
			<method name="duplicate" symbol="jana_time_duplicate">
				<return-type type="JanaTime*"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
				</parameters>
			</method>
			<method name="get_day" symbol="jana_time_get_day">
				<return-type type="guint8"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
				</parameters>
			</method>
			<method name="get_daylight" symbol="jana_time_get_daylight">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
				</parameters>
			</method>
			<method name="get_hours" symbol="jana_time_get_hours">
				<return-type type="guint8"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
				</parameters>
			</method>
			<method name="get_isdate" symbol="jana_time_get_isdate">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
				</parameters>
			</method>
			<method name="get_minutes" symbol="jana_time_get_minutes">
				<return-type type="guint8"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
				</parameters>
			</method>
			<method name="get_month" symbol="jana_time_get_month">
				<return-type type="guint8"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
				</parameters>
			</method>
			<method name="get_offset" symbol="jana_time_get_offset">
				<return-type type="glong"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
				</parameters>
			</method>
			<method name="get_seconds" symbol="jana_time_get_seconds">
				<return-type type="guint8"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
				</parameters>
			</method>
			<method name="get_tzname" symbol="jana_time_get_tzname">
				<return-type type="gchar*"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
				</parameters>
			</method>
			<method name="get_year" symbol="jana_time_get_year">
				<return-type type="guint16"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
				</parameters>
			</method>
			<method name="set_day" symbol="jana_time_set_day">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
					<parameter name="day" type="gint"/>
				</parameters>
			</method>
			<method name="set_hours" symbol="jana_time_set_hours">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
					<parameter name="hours" type="gint"/>
				</parameters>
			</method>
			<method name="set_isdate" symbol="jana_time_set_isdate">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
					<parameter name="isdate" type="gboolean"/>
				</parameters>
			</method>
			<method name="set_minutes" symbol="jana_time_set_minutes">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
					<parameter name="minutes" type="gint"/>
				</parameters>
			</method>
			<method name="set_month" symbol="jana_time_set_month">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
					<parameter name="month" type="gint"/>
				</parameters>
			</method>
			<method name="set_offset" symbol="jana_time_set_offset">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
					<parameter name="offset" type="glong"/>
				</parameters>
			</method>
			<method name="set_seconds" symbol="jana_time_set_seconds">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
					<parameter name="seconds" type="gint"/>
				</parameters>
			</method>
			<method name="set_tzname" symbol="jana_time_set_tzname">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
					<parameter name="tzname" type="gchar*"/>
				</parameters>
			</method>
			<method name="set_year" symbol="jana_time_set_year">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
					<parameter name="year" type="gint"/>
				</parameters>
			</method>
			<vfunc name="duplicate">
				<return-type type="JanaTime*"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
				</parameters>
			</vfunc>
			<vfunc name="get_day">
				<return-type type="guint8"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
				</parameters>
			</vfunc>
			<vfunc name="get_daylight">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
				</parameters>
			</vfunc>
			<vfunc name="get_hours">
				<return-type type="guint8"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
				</parameters>
			</vfunc>
			<vfunc name="get_isdate">
				<return-type type="gboolean"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
				</parameters>
			</vfunc>
			<vfunc name="get_minutes">
				<return-type type="guint8"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
				</parameters>
			</vfunc>
			<vfunc name="get_month">
				<return-type type="guint8"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
				</parameters>
			</vfunc>
			<vfunc name="get_offset">
				<return-type type="glong"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
				</parameters>
			</vfunc>
			<vfunc name="get_seconds">
				<return-type type="guint8"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
				</parameters>
			</vfunc>
			<vfunc name="get_tzname">
				<return-type type="gchar*"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
				</parameters>
			</vfunc>
			<vfunc name="get_year">
				<return-type type="guint16"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
				</parameters>
			</vfunc>
			<vfunc name="set_day">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
					<parameter name="day" type="gint"/>
				</parameters>
			</vfunc>
			<vfunc name="set_hours">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
					<parameter name="hours" type="gint"/>
				</parameters>
			</vfunc>
			<vfunc name="set_isdate">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
					<parameter name="isdate" type="gboolean"/>
				</parameters>
			</vfunc>
			<vfunc name="set_minutes">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
					<parameter name="minutes" type="gint"/>
				</parameters>
			</vfunc>
			<vfunc name="set_month">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
					<parameter name="month" type="gint"/>
				</parameters>
			</vfunc>
			<vfunc name="set_offset">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
					<parameter name="offset" type="glong"/>
				</parameters>
			</vfunc>
			<vfunc name="set_seconds">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
					<parameter name="seconds" type="gint"/>
				</parameters>
			</vfunc>
			<vfunc name="set_tzname">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
					<parameter name="tzname" type="gchar*"/>
				</parameters>
			</vfunc>
			<vfunc name="set_year">
				<return-type type="void"/>
				<parameters>
					<parameter name="self" type="JanaTime*"/>
					<parameter name="year" type="gint"/>
				</parameters>
			</vfunc>
		</interface>
	</namespace>
</api>
