AC_PREREQ(2.53)
AC_INIT(jana-bindings, 0.0, http://www.openedhand.com/)
AM_INIT_AUTOMAKE()
AM_CONFIG_HEADER(config.h)
AM_MAINTAINER_MODE

AC_ARG_ENABLE([vala],
	AC_HELP_STRING([--enable-vala],
		[Enable building of vala bindings]),
		[build_vala=vala])

AC_PATH_PROG(VALAC, valac, no)
AC_PATH_PROG(VAPIGEN, vapigen, no)
PKG_PROG_PKG_CONFIG()

if test "$build_vala" = "vala"; then
	if test "x$VALAC" = "xno"; then
		AC_MSG_ERROR([Cannot find the "valac" compiler in your PATH])
	fi
	if test "x$VAPIGEN" = "xno"; then
		AC_MSG_ERROR([Cannot find the "vapigen" binary in your PATH])
	fi
	if test "x$PKG_CONFIG" = "xno"; then
		AC_MSG_ERROR([Cannot find the "pkg-config" program in your PATH])
	fi

	PKG_CHECK_MODULES(VALA, [vala-1.0 >= 0.3.1])

	m4_define([default_vapidir], [${datarootdir}/jana/vala/vapi])
	AC_ARG_WITH([vapidir],
		[AC_HELP_STRING([--with-vapidir=@<:@default_vapidir@:>@],
			[define where to install the VAPI files])],
		[VAPIDIR=$with_vapidir],
		[VAPIDIR=default_vapidir])

AC_SUBST(VAPIDIR)

fi

AC_SUBST([build_vala])
AC_SUBST([VAPIDIR])

AC_OUTPUT([
Makefile
vala/Makefile
vala/jana-vala.pc
])
