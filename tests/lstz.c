/*
 * Author: Chris Lord <chris@linux.intel.com>
 * Copyright (c) 2007 OpenedHand Ltd
 * Copyright (C) 2008 - 2009 Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 2.1, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 */


#include <glib.h>
#include <libical/ical.h>

/* List ical time-zones. Compile with:
 * gcc -o lstz lstz.c `pkg-config --cflags --libs glib-2.0 libecal-1.2` -Wall
 */

int
main (int argc, char **argv)
{
	gint i;
	icaltimetype today = icaltime_today ();
	icalarray *builtin = icaltimezone_get_builtin_timezones ();
	for (i = 0; i < builtin->num_elements; i++) {
		icaltimezone *zone = (icaltimezone *)icalarray_element_at (
			builtin, i);
		gchar *zone_tz;
		gdouble offset = ((gdouble)icaltimezone_get_utc_offset (
			zone, &today, NULL)/60.0)/60.0;
		zone_tz = icaltimezone_get_tznames (zone);

		g_print ("%s (%s, +%lf)\n",
			icaltimezone_get_display_name (zone),
			icaltimezone_get_tznames (zone),
			offset);
	}

	return 0;
}

